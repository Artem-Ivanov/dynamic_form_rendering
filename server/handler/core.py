import json
import time
import tornado.web
import tornado.httputil


class BaseHandler(tornado.web.RequestHandler):
    """
    Класс, обрабатывающий все запросы
    """

    entity_manager = None
    commands = dict()

    def set_default_headers(self):
        """
        Добавляем headers в ответы по умолчанию
        :return:
        """
        self.set_header('Connection', 'keep-alive')
        self.set_header('Content-Type', 'text/html; charset=UTF-8')
        self.set_header('Date', tornado.httputil.format_timestamp(time.time()))
        self.set_header('Server', 'nginx/1.10.1')

    def data_received(self, chunk):
        """
        Перегружаем абстрактный метод
        :param chunk:
        :return:
        """
        return None
    
    def save_request(self, result):
        """
        Сохраняем запрос в хранилище
        :param result: Данные для ответа на клиент
        :return:
        """
        url = self.request.path
        if self.request.method == 'POST':
            url = '{0}/{1}'.format(url, result.get('id', ''))

        self.add_header('Location', url)
        self.set_status(303)

    def send_request(self, code, result):
        """
        Отсылаем ответ на запрос
        :param code: Код ответа
        :param result: Данные для ответа на клиент
        :return:
        """
        self.set_status(code)
        self.finish(json.dumps(result))

    def get(self, _id=None, *args):
        """
        Обработчик GET-запросов
        :param _id: Id сущности
        :return:
        """
        if not self.entity_manager:
            self.finish({})
            return
        obj = self.entity_manager.read(_id=_id)
        self.send_request(code=200, result=obj)

    def post(self, *args):
        """
        Обработчик POST-запросов
        :return:
        """
        obj = self.entity_manager.create()
        self.save_request(result=obj)
