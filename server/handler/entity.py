import tornado.escape

from server.handler.core import BaseHandler
from server.manager.entity import entities_manager


class EntityHandler(BaseHandler):
    """
    Класс, обрабатывающий все запросы на Sales
    """
    entity_manager = entities_manager

    commands = dict(
    )

    def post(self, *args):
        """
        Обработчик POST-запросов
        :return:
        """
        _id, command = self.path_args
        _id = None if not _id else _id
        data = dict()
        if self.request.body:
            data = tornado.escape.json_decode(self.request.body)
        method = self.commands.get(command)
        if not method:
            return

        _id = getattr(self.entity_manager, method)(_id=_id, **data)
        url = '{0}://{1}/{2}/{3}'.format(self.request.protocol, self.request.host, self.request.path.split('/')[1], _id)

        self.add_header('Location', url)
        self.set_status(303)
