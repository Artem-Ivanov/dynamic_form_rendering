import os
import json


class CoreManager(object):
    """
    Базовый класс-менеджер, управляющий доступом к данным
    """

    tag = str()
    lookup_map = dict()
    entity = dict()
    line = dict()
    child = list()

    def read(self, _id=None) -> list:
        """
        Прочитать сущность/сущности
        :param _id: Идентификатор
        :return:
        """
        if _id:
            item = self.get_item(_id=_id)
            if item:
                item = self.lookup(item)
            return item
        return self.get_list()

    def create(self, **kwargs) -> dict:
        """
        Создать новую сущность
        :return:
        """
        item = self.init_item(**kwargs)
        entities = self.get_list()
        entities.append(item)
        self.set_list(entities=entities)
        item = self.lookup(item)
        return item

    def get_list(self) -> list:
        """
        Получить список сущностей из хранилища
        :return:
        """
        if self.child:
            return self.get_combo_list()

        with open(self.get_filename()) as f:
            s = f.read()
            data = json.loads(s)
            return self.get_modified_data(data)

    def set_list(self, entities: list) -> None:
        """
        Записать экземпляр сущности в хранилище
        :param entities: Экземпляры сущностей
        :return:
        """
        with open(self.get_filename(), 'w') as f:
            dump_params = dict(
                ensure_ascii=False,
                indent=4,
                sort_keys=True
            )
            s = json.dumps(entities, **dump_params)
            f.write(s)

    def get_combo_list(self) -> list:
        """
        Получить список сущностей из хранилища
        :return:
        """
        res = []
        for children in self.child:
            res += children.get_list()
        return res

    def get_item(self, _id: str) -> dict:
        """
        Получить экземпляр сущности из хранилища
        :param _id: Идентификатор
        :return:
        """
        entities = self.get_list()
        for item in entities:
            if item.get('id') == _id:
                return item


    def init_item(self, **kwargs) -> dict:
        """
        Инициировать данные для новой сущности
        :return:
        """
        item = self.entity.copy()
        item['id'] = self.get_next_identifier()
        return item

    def get_next_identifier(self) -> str:
        """
        Получить новый идентификатор
        :return:
        """
        entities = self.get_list()
        max_id = max([int(x['id']) for x in entities], default=0)
        return '{0}'.format(max_id + 1)

    def lookup(self, item: dict) -> dict:
        """
        Разлукапить прочитанную сущность
        :param item:
        :return:
        """
        for key in set(item.keys()) & set(self.lookup_map.keys()):
            mgr = self.lookup_map[key]

            if isinstance(item[key], list):
                item[key] = [mgr.get_item(_id=x) for x in item[key]]
                continue

            item[key] = mgr.get_item(_id=item[key])
        return item

    def get_modified_data(self, data: list) -> list:
        """
        Модифицируем данные перед выдачей
        :param data: Данные
        :return:
        """
        fileName = 'server/json/property.json'

        with open(fileName) as f:
            s = f.read()
            entityField = json.loads(s)

        for i in data:
            i['properties'] = (entityField * int(i['count']))

        return data

    def get_filename(self) -> str:
        """
        Получить имя файла.
        :return:
        """
        fullname = 'server/json/{0}.json'.format(self.tag)

        if not os.path.exists(fullname):
            self.init_json_storage(filename=fullname)

        return fullname

    def init_json_storage(self, filename: str) -> None:
        """
        Если файла-хранилища нет, то создаем его, инициируем
        :return:
        """
        with open(filename, 'w') as f:
            dump_params = dict(
                ensure_ascii=False,
                indent=4,
                sort_keys=True
            )
            data = self.get_init_storage_data()
            s = json.dumps(data, **dump_params)
            f.write(s)

    @staticmethod
    def get_init_storage_data() -> list:
        """
        Инициируем данные для хранилища
        :return:
        """
        return []

    def execute_command(self, command: str, _id: int=None, values: dict=None):
        """
        Испольняем комманду пришедшую с клиента
        :param command: Название комманды
        :param _id: Идентификатор сущности
        :param values: Данные пришедшие вместе с коммандой
        :return:
        """
        f = getattr(self, command)
        if not f:
            return

        return f(_id=_id, data=values)
