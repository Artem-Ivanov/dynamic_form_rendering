from server.manager.core import CoreManager


class EntityManager(CoreManager):
    tag = 'entities'

    entity = dict(
        type=dict(id=str()),
        properties=list(),
    )

    def read(self, _id=None, **kwargs):
        result = super().read(_id, **kwargs)
        if kwargs and isinstance(result, list):
            type_list = [x.decode('utf-8') for x in kwargs['type']]
            filtered = list()
            for element in result:
                if 'type' in element:
                    type_id = element['type']['id']
                    if type_id in type_list:
                        filtered.append(element)
            return filtered
        else:
            return result

entities_manager = EntityManager()
