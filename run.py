import os

import tornado.web
import tornado.wsgi
import tornado.ioloop
import tornado.httpserver

from server.handler.core import BaseHandler
from server.handler.entity import EntityHandler


class MainHandler(BaseHandler):
    """
    Класс, управляющий отдачей контента основной страницы
    """
    def get(self, _id=None, *args):
        """
        Отдает основную страницу
        :param _id: Не используется
        :return:
        """
        self.render('app/index.html')


class Application(tornado.web.Application):
    """
    Приложение на Tornado
    """
    def __init__(self):
        settings = {
            'compress_response': True,
            'autoreload': True,
            'debug': True
        }
        handlers = (
            (r'/', MainHandler),
            (r'/entities/?([0-9]*)/?([a-z_]*)', EntityHandler),
            (r'/(.*)', tornado.web.StaticFileHandler, {'path': os.getcwd() + '/app'}),
        )
        tornado.web.Application.__init__(self, handlers, **settings)


application = Application()


if __name__ == '__main__':

    server = tornado.httpserver.HTTPServer(application)
    server.bind(port=8500)
    server.start(1)
    tornado.ioloop.IOLoop.current().start()