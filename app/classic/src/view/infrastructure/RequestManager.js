Ext.define('DynamicFormRendering.view.infrastructure.RequestManager', {
    extend: 'Ext.data.Store',

    /**
     * Обработчик успешного ответа от сервера на основной адрес
     * @param callback Функиця-возврата
     * @returns {Function}
     */
    command_success_handler: function(callback) {
        return function(response) {
            if (response.status === 200) {
                if (response.responseText) {
                    var obj = Ext.JSON.decode(response.responseText);
                    if (obj) {
                        if (obj['tsx8yt']) { // Для заглушки
                            obj = obj['data']
                        }
                        callback(obj);
                    } else {
                        callback(true);
                    }
                } else {
                    callback(true);
                }
            }
        }
    },

    /**
     * Обработчик провального ответа от сервера на адрес таска
     * @returns {Function}
     */
    command_failure_handler: function() {
        return function(response) {
            console.log(response.status, response.statusText)
        }
    },

    /**
     * Получаем дополнительные параметры запроса
     * @param route Роутинг
     * @param data Данные
     * @returns {{method: *}}
     */
    get_extra_command_params: function(route, data) {
        var http_method = data['task'] ?
            'POST' :
            'GET';

        var result = {
            'method': http_method,
            'url': route
        };

        if (data['entity_id']) {
            result['url'] = result['url'] + '/' + data['entity_id'];
            delete  data['entity_id'];
        }

        if (data['line_id']) {
            data['id'] = data['line_id'];
            delete data['line_id'];
        }

        if (data['task']) {
            result['url'] = result['url'] + '/' + data['task'];
            delete  data['task'];
        }

        if (http_method === 'POST' && data) {
            result['jsonData'] = data;
        }

        return result;
    },

    /**
     * Отправляем основной запрос на сервер
     * @param route Роутинг
     * @param data Данные
     * @param callback Функция возврата
     */
    send: function(route, data, callback) {
        var request_params = {
            success: this.command_success_handler(callback),
            failure: this.command_failure_handler()
        };

        var extra_params = this.get_extra_command_params(route, data);
        Ext.Object.merge(request_params, extra_params);

        Ext.Ajax.request(request_params);
    }
});