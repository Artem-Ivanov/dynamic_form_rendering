Ext.define('DynamicFormRendering.view.main.Form', {
    extend: 'Ext.form.Panel',

    xtype: 'form',
    itemId: 'form',

    requires: [
        'Ext.form.field.Text',
        'Ext.layout.container.Table'
    ],

    scrollable: 'y',

    fieldDefaults: {
        labelWidth: 80
    },

    defaults: {
        xtype: 'textfield',
        margin: '0 50 10 0'
    },

    layout: {
        type: 'vbox',
        columns: 2,
        align: 'stretch'
    },

    padding: 0,
    bodyPadding: 20,

    items: [
        {
            xtype: 'fieldset',
            title: 'Properties',
            collapsible: true,
            defaults: {
                labelWidth: 90,
                anchor: '100%',
                layout: 'vbox'
            }
        }
    ]
});