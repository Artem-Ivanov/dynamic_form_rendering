Ext.define('DynamicFormRendering.view.main.Grid', {
    extend: 'Ext.grid.Panel',

    xtype: 'grid',
    itemId: 'grid',

    requires: [
        'Ext.grid.column.RowNumberer',
        'Ext.grid.plugin.RowEditing'
    ],

    border: false,

    scrollable: 'y',
    flex: 1,

    store: {
        fields: ['id', 'property', 'type', 'properties']
    },

    columns: [
        {
            text: 'ИД',
            dataIndex: 'id',
            editor: 'textfield',
            align: 'left',
            width: '10%'
        },
        {
            text: 'Наименование',
            dataIndex: 'property',
            editor: 'textfield',
            align: 'left',
            flex:1,
            renderer: function(property){
                return property.name;
            }
        },
        {
            text: 'Количество',
            dataIndex: 'properties',
            editor: 'textfield',
            align: 'left',
            width: '25%',
            renderer: function(type){
                return type.length;
            }
        }
    ]
});