
Ext.define('DynamicFormRendering.view.main.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'main',

    requires: [
        'DynamicFormRendering.view.main.Controller',
        'DynamicFormRendering.view.main.Grid',
        'DynamicFormRendering.view.main.Form',
        'Ext.layout.container.VBox'
    ],

    controller: 'main',

    layout: 'border',
    flex: 1,

    bodyBorder: false,

    defaults: {
        collapsible: true,
        split: true,
        margin: '5 0 0 0'
    },

    items: [
        {
            xtype: 'grid',
            title: 'List view',
            region:'west',
            floatable: false

        },
        {
            xtype: 'form',
            title: 'Detail view',
            collapsible: false,
            region: 'center'
        }
    ]
});
