Ext.define('DynamicFormRendering.view.main.Controller', {
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.main',

    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.Display',
        'Ext.form.field.Text'
    ],

    uses: [
        'DynamicFormRendering.view.infrastructure.RequestManager'
    ],

    control: {
        '#grid': {
            rowdblclick: 'onRowDblClick'
        }
    },

    init: function() {
        this.load();
    },

    load: function () {
        let grid = this.getView();
        grid.setLoading('Загружаем данные...');

        let repository = this.getRepository(),
            callback = Ext.Function.bind(this.onLoadDataComplete, this);

        repository.send('entities', {}, callback);
    },

    onLoadDataComplete: function (records) {
        let grid = this.getView().down('grid');
        grid.getStore().loadRawData(records);
        grid.getSelectionModel().select(0);
        grid.getView().refresh();

        grid.setLoading(false);
    },

    onRowDblClick: function (tableview, record) {
        this.setData(record);
    },

    setData: function(data) {
        let form = this.getView().down('form').down('fieldset'),
            time = new Date();
        form.removeAll();

        let control = [];

        Ext.Array.forEach(data.data.properties, function (item) {

            if (item.type === 'date'){
                control.push(this.getDateFieldControl(item));
            } else if (item.type === 'string') {
               control.push(this.getDisplayFieldControl(item));
            } else {
                control.push(this.getTextFieldControl(item));
            }
        }, this);

        form.add(control);

        let delta = (new Date() - time) / 1000;
        console.log('Delta: ' + delta + 'sec');
        console.log('Count: ' + data.data.properties.length);
    },

    getTextFieldControl: function(item) {
        return {
            xtype: 'textfield',
            fieldLabel: item.name,
            name: item.name,
            value: item.value
        }
    },

    getDisplayFieldControl: function(item) {
        return {
            xtype: 'displayfield',
            fieldLabel: item.name,
            name: item.name,
            value: item.value
        }
    },

    getDateFieldControl: function(item) {
        return {
            xtype: 'datefield',
            anchor: '100%',
            format: 'd.m.Y',
            fieldLabel: item.name,
            name: item.name,
            value: item.value
        }
    },

    getItems: function() {
        return this.getView() ? this.getView().items.items : [];
    },

    getRepository: function () {
        return Ext.create('DynamicFormRendering.view.infrastructure.RequestManager');
    }
});