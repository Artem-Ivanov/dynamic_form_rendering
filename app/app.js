/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'DynamicFormRendering.Application',

    name: 'DynamicFormRendering',

    requires: [
        // This will automatically load all classes in the DynamicFormRendering namespace
        // so that application classes do not need to require each other.
        'DynamicFormRendering.*'
    ],

    // The name of the initial view to create.
    mainView: 'DynamicFormRendering.view.main.Main'
});
